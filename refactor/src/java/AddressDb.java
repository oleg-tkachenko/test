import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import static java.util.Objects.nonNull;

public class AddressDb {
    private static final String ADD_PERSON_STATEMENT = "INSERT INTO AddressEntry VALUES (?, ?, ?)";
    private static final String FIND_PERSON_STATEMENT = "SELECT * FROM AddressEntry WHERE name = ?";
    private static final String FIND_PERSONS_WHO_HAS_MOBILE_STATEMENT = "SELECT * FROM AddressEntry WHERE phoneNumber LIKE ?";
    private static final String GET_ALL_STATEMENT = "SELECT * FROM AddressEntry";
    private static final String GET_NAMES_BY_LENGTH_STATEMENT = "SELECT name FROM AddressEntry WHERE LENGTH(name) <= ?";
    private static final String GET_SIZE_STATEMENT = "SELECT COUNT(*) FROM AddressEntry";
    /**
     * Параметры подключения к БД
     */
    private static final String JDBC_DRIVER = "oracle.jdbc.ThinDriver";
    private static final String DB_URL = "jdbc:oracle:thin:@prod";
    private static final String DB_USERNAME = "admin";
    private static final String DB_PASSWORD = "beefhead";

    private static AddressDb addressDb = null;

    private static Logger log = Logger.getLogger(AddressDb.class.getName());

    private AddressDb() {
    }

    /**
     * Будет возвращен всегда один и тот же обьект
     *
     * @return AddressDb
     */
    public synchronized static AddressDb getInstance() {
        if (addressDb == null) {
            addressDb = new AddressDb();
        }
        return addressDb;
    }

    /**
     * @param person обьект Person для сохранения в БД,
     *               если <tt>NULL</tt> сохранение не происходит.
     */
    public void addPerson(Person person) {
        if (nonNull(person)) {
            try (Connection connection = getConnection()) {
                try (PreparedStatement statement = connection.prepareStatement(ADD_PERSON_STATEMENT)) {

                    statement.setLong(1, System.currentTimeMillis());
                    statement.setString(2, person.getName());
                    statement.setString(3, person.getPhoneNumber());

                    statement.executeUpdate();
                } catch (SQLException e) {
                    connection.rollback();
                    log.warning("Error while adding Person: " + e.getMessage());
                }
            } catch (SQLException e) {
                log.severe(e.getMessage());
            }
        }
    }

    /**
     * @param name имя человека для поиска в БД
     * @return обект Person если у такового поле name соответствует заданному,
     * <tt>NULL</tt> если такого имени нет в БД
     */
    public Person findPerson(String name) {
        Person person = null;
        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(FIND_PERSON_STATEMENT)) {
                statement.setString(1, name);
                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    String rs_name = rs.getString("name");
                    String rs_phone = rs.getString("phoneNumber");
                    person = new Person(rs_name, rs_phone);
                }
            }
        } catch (SQLException e) {
            log.warning("Error while finding Person: " + e.getMessage());
        }
        return person;
    }

    /**
     * @param maxLength максимальная длинна имени для выборки
     * @return список имен людей у которых длинна имени не превышает заданную
     */
    public List<String> getNamesByLength(int maxLength) {
        List<String> names = new ArrayList<>();
        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(GET_NAMES_BY_LENGTH_STATEMENT)) {
                statement.setInt(1, maxLength);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    String rs_name = rs.getString("name");
                    names.add(rs_name);
                }
            }
        } catch (SQLException e) {
            log.warning("Error while getNamesByLength list: " + e.getMessage());
        }
        return names;
    }

    /**
     * @param pref Код мобильного оператора
     * @return список людей у которых номер телефона начинается с заданного кода
     */
    public List<Person> getPersonsWhoHasMobile(String pref) {
        String prefix = pref.concat("%");
        List<Person> persons = new ArrayList<>();
        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(FIND_PERSONS_WHO_HAS_MOBILE_STATEMENT)) {
                statement.setString(1, prefix);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    String rs_name = rs.getString("name");
                    String rs_phone = rs.getString("phoneNumber");
                    persons.add(new Person(rs_name, rs_phone));
                }
            }
        } catch (SQLException e) {
            log.warning("Error while getNamesByLength list: " + e.getMessage());
        }
        return persons;
    }

    /**
     * @return кол-во записей в таблице AddressEntry
     */
    public int getAddressBookSize() {
        int count = 0;
        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(GET_SIZE_STATEMENT)) {
                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            log.warning("Error while getting size of AddressBook: " + e.getMessage());
        }
        return count;
    }

    /**
     * @return список всех людей из таблицы БД
     */
    public List<Person> getAll() {
        List<Person> persons = new LinkedList<>();
        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(GET_ALL_STATEMENT)) {
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    String rs_name = rs.getString("name");
                    String rs_phone = rs.getString("phoneNumber");
                    persons.add(new Person(rs_name, rs_phone));
                }
            }
        } catch (SQLException e) {
            log.warning("Error while getting all persons: " + e.getMessage());
        }
        return persons;
    }

    /**
     * @return соединение с БД
     */
    private Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager
                    .getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            log.severe("Error while getting Connection: " + e.getMessage());
        }
        return connection;
    }

}
