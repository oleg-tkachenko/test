import java.util.List;

public class AddressBook {
    /**
     * 070 - код мобильного оператора в Швейцарии
     */
    private static final String MOBILE_NUMBER_PREFIX = "070";

    private AddressDb addressDb = AddressDb.getInstance();

    /**
     * @param name имя, критерий поиска
     * @return <tt>TRUE</tt> если номер человека начинается на 070, иначе
     * <tt>FALSE</tt> также если человека с таким именем нет в БД
     */
    public boolean hasMobile(String name) {
        String mobileNumber = getMobileNumberByName(name);
        return mobileNumber != null && mobileNumber.startsWith(MOBILE_NUMBER_PREFIX);
    }

    /**
     * @return кол-во записей в адресной книге
     */
    public int getSize() {
        return addressDb.getAddressBookSize();
    }

    /**
     * @param name имя, критерий поиска
     * @return номер телефона, если человек с таким именем есть в БД,
     * иначе <tt>NULL</tt>
     */
    public String getMobileNumberByName(String name) {
        Person person = addressDb.findPerson(name);
        return person != null ? person.getPhoneNumber() : null;
    }

    /**
     * @param maxLength максимальная длина имени
     * @return список имен, чья длина не превышает максимальную
     */
    public List<String> getNamesByLength(int maxLength) {
        return addressDb.getNamesByLength(maxLength);
    }

    /**
     * @param prefix код моб.оператора
     * @return список людей, у которых номер телефона начинается на указанный код
     */
    public List<Person> getPersonsWhoHasMobile(String prefix) {
        return addressDb.getPersonsWhoHasMobile(MOBILE_NUMBER_PREFIX);
    }
}