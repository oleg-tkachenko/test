
public class Person {
    private String name;
    private String phoneNumber;

    /**
     *
     * @param name имя человека
     * @param phoneNumber телефонный номер
     */
    public Person(String name, String phoneNumber) {
        setName(name);
        setPhoneNumber(phoneNumber);
    }

    /**
     *
     * @return имя человека
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name имя человека
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return телефонный номер
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber телефонный номер
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
