package com.oatkachenko.cache.impl;

import com.oatkachenko.cache.CacheMap;
import com.oatkachenko.clock.Clock;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

/**
 * <tt>CacheMap2Iml</tt> - реализация интерфейса <tt>CacheMap</tt>, основанная на HashMap
 * с использованием класса-оболочки для хранения ключа. Класс-оболочка KeyTimer представляет
 * собой класс с двумя полями:
 * - ключ
 * - время создания ключа
 * Функции hashcode и equals переопределены таким образом что работают внутри себя с
 * оригинальным ключом, что позволит избежать конфликтов в процессе работы.
 * Не синхронизирована, порядок добавления элементов не сохраняется и может менятся
 * в процессе работы. За счет применения хеш-функции при определении ячейки хранения
 * - время для операций <tt>get</tt> and <tt>put</tt> - константа. Особенностью
 * данной реализации является удаление элементов по истечению заданного промежутка времени.
 * Это достигается путем использования класса оболочки для ключа. При каждом обращении
 * к кэш запускается(если есть устаревшие эл-ты) процесс очистки утаревших элементов.
 *
 * @param <KeyType>   тип ключа поддерживаемого CacheMapIml
 * @param <ValueType> тип значения записываемого CacheMapIml
 * @author Oleg Tkachenko
 * @see HashMap
 * @since 1.8
 */

public class CacheMap2Impl<KeyType, ValueType> implements CacheMap<KeyType, ValueType> {
    /**
     * cache выполняет функцию хранилища кешируемых данных, реализован на <tt>HashMap</tt>.
     */
    Map<KeyTimer, ValueType> cache = new HashMap<>();
    /**
     * timeToLive переменная определяющая время хранения элемента в кэше, по умолчанию 60000,
     * еденица измерения - миллисекунды.
     */
    private long timeToLive = 60 * 1000;
    /**
     * Время первого устаревшего элемента
     */
    private long firstItemToDel = timeIsNow();

    /**
     * возвращает время хранения элемента в кэше.
     *
     * @return long
     */
    public long getTimeToLive() {
        return timeToLive;
    }

    /**
     * устанавливает время хранения элемента в кэше.
     *
     * @param timeToLive значение типа long, миллисекунды
     */
    public void setTimeToLive(long timeToLive) {
        this.timeToLive = timeToLive;
    }

    /**
     * добавляет элемент в кэш.
     *
     * @param key   - ключ для доступа к значению не может быть <tt>null</tt>
     * @param value - хранимое значение, может быть <tt>null</tt>
     * @return предыдущее значение ассоциированное с <tt>key</tt>,
     * или <tt>null</tt> если с данным ключом не было
     * сопоставлено ни одного элемента, также <tt>null</tt>
     * может быть возвращен если с ключом был ассоциирован <tt>null</tt>.
     */
    public ValueType put(KeyType key, ValueType value) {
        clearExpired();                                 //очистка кэш перед добавлением
        return cache.put(wrapKey(key), value);                   //кладем эл-т в кэш
    }

    /**
     * удаляет элементы из кэша, время хранения которых истекло.
     */
    public void clearExpired() {
        if (firstItemToDel < timeIsNow()) { //есть ли устаревший эл-т? исключает "холостые" проходы по Map
            for (Iterator<Map.Entry<KeyTimer, ValueType>> it = cache.entrySet().iterator(); it.hasNext(); ) {
                KeyTimer keyTimer = it.next().getKey();
                if ((keyTimer.getKeyCreated() + timeToLive) < timeIsNow()) {     // устаревший ли это элемент
                    it.remove();                        // удаление из кэш
                } else {
                    // среди эл-тов не подлежащих удалению выбираем эл-т с минимальным значением времени
                    firstItemToDel = keyTimer.getKeyCreated() < firstItemToDel ? keyTimer.getKeyCreated() : firstItemToDel;
                }
            }
        }
    }

    /**
     * очищает кэш
     */
    public void clear() {
        cache.clear();
    }

    /**
     * @param key ключ, наличие которого будет проверено в кэше.
     * @return true если в кэше есть данный ключом.
     */
    public boolean containsKey(Object key) {
        clearExpired();
        return cache.containsKey(wrapKey(key));
    }

    /**
     * @param value значение, наличие которого будет проверено в кэше.
     * @return true если в кэше есть данное значение.
     */
    public boolean containsValue(Object value) {
        clearExpired();
        return cache.containsValue(value);
    }

    /**
     * Возвращает значение сопоставимое с заданным ключом, если
     * в кэше нет элемента с таким ключем будет возвращен <tt>null</tt>.
     *
     * @param key ключ для доступа к значению.
     * @return значение сопоставимое с ключом
     */
    public ValueType get(Object key) {
        clearExpired();
        return cache.get(wrapKey(key));
    }

    /**
     * @return true если в кэше не содержится записей
     */
    public boolean isEmpty() {
        clearExpired();
        return cache.isEmpty();
    }

    /**
     * @param key ключ для доступа к удаляемому элементу
     * @return возвращает элемент ассоциированный с заданным ключом
     */
    public ValueType remove(Object key) {
        clearExpired();
        return cache.remove(wrapKey(key));
    }

    /**
     * @return количество элементов ключ-значение в кэше
     */
    public int size() {
        clearExpired();
        return cache.size();
    }

    /**
     * @return возвращает текущее время.
     */
    private Long timeIsNow() {
        return Clock.getTime();
    }

    /**
     * Метод позволяет "оборачивать" внешний ключ в обьект, который используется
     * в кэш в виде ключа.
     *
     * @param key внешний ключ для работы с кэш
     * @return обьект-оболочка для ключа
     */
    public KeyTimer wrapKey(Object key) {
        return new KeyTimer((KeyType) key);
    }

    /**
     * Внутренний класс-оболочка
     */
    private class KeyTimer {
        // поле для хранения время когда обьект был создан
        private long keyCreated;
        // внешний ключ
        private KeyType key;

        /**
         * @param originalKey внешний ключ
         */
        public KeyTimer(KeyType originalKey) {
            this.keyCreated = timeIsNow();
            this.key = originalKey;
        }

        public long getKeyCreated() {
            return keyCreated;
        }

        public KeyType getKey() {
            return key;
        }

        /**
         * @return число-представление обьекта типа int,
         * может вернуть 0 в случае, если key == NULL
         */
        @Override
        public int hashCode() {
            return Objects.hashCode(key);
        }

        /**
         * @param obj ключ для доступа к значению из кэш
         * @return true - если ключ из аргумента и из кэш совпадают
         */
        @Override
        public boolean equals(Object obj) {
            return Objects.equals(key, ((KeyTimer) obj).getKey());
        }
    }
}