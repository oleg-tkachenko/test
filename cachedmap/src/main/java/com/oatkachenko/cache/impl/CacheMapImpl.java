package com.oatkachenko.cache.impl;

import com.oatkachenko.cache.CacheMap;
import com.oatkachenko.clock.Clock;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * <tt>CacheMapIml</tt> - реализация интерфейса <tt>CacheMap</tt>, основанная на <tt>HashMap.</tt>
 * Не синхронизирована, порядок добавления элементов * не сохраняется и может менятся
 * в процессе работы. За счет применения хеш-функции при определении ячейки хранения
 * - время для операций <tt>get</tt> and <tt>put</tt> - константа. Особенностью
 * данной реализации является удаление элементов по истечению заданного промежутка времени.
 * Это достигается путем использования дополнительного "хранилища" в виде <tt>HashMap</tt>.
 * В качестве ключа используется чило типа Long - время, при наступлении которого элемент
 * считается устаревшим и подлежит удалению, в качестве значения - "ключ" того же типа, что
 * и в кэше.
 *
 * @param <KeyType>   тип ключа поддерживаемого CacheMapIml
 * @param <ValueType> тип значения записываемого CacheMapIml
 * @author Oleg Tkachenko
 * @see HashMap
 * @since 1.8
 */

public class CacheMapImpl<KeyType, ValueType> implements CacheMap<KeyType, ValueType> {
    /**
     * cache выполняет функцию хранилища кешируемых данных, реализован на <tt>HashMap</tt>.
     */
    Map<KeyType, ValueType> cache = new HashMap<>();

    /**
     * itemsToBeDeleted выполняет функцию вспомогательного хранилища, реализован на <tt>HashMap</tt>.
     * Используется для хранения времени, по наступлении которого будет удален элемент из кэша,
     * и ключа, по которому будет удален элемент.
     */
    Map<Long, KeyType> itemsToBeDeleted = new HashMap<>();

    /**
     * timeToLive переменная определяющая время хранения элемента в кэше, по умолчанию 60000,
     * еденица измерения - миллисекунды.
     */
    private long timeToLive = 60 * 1000;

    /**
     * Время первого устаревшего элемента
     */
    private long firstItemToDel = timeIsNow();

    /**
     * возвращает время хранения элемента в кэше.
     *
     * @return long
     */
    public long getTimeToLive() {
        return timeToLive;
    }

    /**
     * устанавливает время хранения элемента в кэше.
     *
     * @param timeToLive значение типа long, миллисекунды
     */
    public void setTimeToLive(long timeToLive) {
        this.timeToLive = timeToLive;
    }

    /**
     * добавляет элемент в кэш.
     *
     * @param key   - ключ для доступа к значению не может быть <tt>null</tt>
     * @param value - хранимое значение, может быть <tt>null</tt>
     * @return предыдущее значение ассоциированное с <tt>key</tt>,
     * или <tt>null</tt> если с данным ключом не было
     * сопоставлено ни одного элемента, также <tt>null</tt>
     * может быть возвращен если с ключом был ассоциирован <tt>null</tt>.
     */
    public ValueType put(KeyType key, ValueType value) {
        clearExpired();                                 //очистка кэш перед добавлением
        long timeWhenDelete = timeIsNow() + timeToLive; //время когда эл-т считается устаревшим
        itemsToBeDeleted.put(timeWhenDelete, key);      //записываем в хранилище время и ключ для удаления
        return cache.put(key, value);                   //кладем эл-т в кэш
    }

    /**
     * удаляет элементы из кэша, время хранения которых истекло.
     */
    public void clearExpired() {
        if (firstItemToDel < timeIsNow()) {             // есть ли устаревший элемент? исключает "холостые" проходы по Map
            firstItemToDel = timeIsNow() + timeToLive;               //устанавливаем максимально возможное время
            // Запускаем итератор по эл-там подлежащим удалению
            for (Iterator<Map.Entry<Long, KeyType>> it = itemsToBeDeleted.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<Long, KeyType> entry = it.next();
                if (entry.getKey() < timeIsNow()) {     // устаревший ли это элемент
                    cache.remove(entry.getValue());     // удаление из кэш
                    it.remove();                        // удаление из вспомогательного хранилища
                } else {
                    // среди эл-тов не подлежащих удалению выбираем эл-т с минимальным значением времени
                    firstItemToDel = entry.getKey() < firstItemToDel ? entry.getKey() : firstItemToDel;
                }
            }
        }
    }

    /**
     * очищает кэш
     */
    public void clear() {
        cache.clear();
    }

    /**
     * @param key ключ, наличие которого будет проверено в кэше.
     * @return true если в кэше есть данный ключом.
     */
    public boolean containsKey(Object key) {
        clearExpired();
        return cache.containsKey(key);
    }

    /**
     * @param value значение, наличие которого будет проверено в кэше.
     * @return true если в кэше есть данное значение.
     */
    public boolean containsValue(Object value) {
        clearExpired();
        return cache.containsValue(value);
    }

    /**
     * Возвращает значение сопоставимое с заданным ключом, если
     * в кэше нет элемента с таким ключем будет возвращен <tt>null</tt>.
     *
     * @param key ключ для доступа к значению.
     * @return значение сопоставимое с ключом
     */
    public ValueType get(Object key) {
        clearExpired();
        return cache.get(key);
    }

    /**
     * @return true если в кэше не содержится записей
     */
    public boolean isEmpty() {
        clearExpired();
        return cache.isEmpty();
    }

    /**
     * @param key ключ для доступа к удаляемому элементу
     * @return возвращает элемент ассоциированный с заданным ключом
     */
    public ValueType remove(Object key) {
        clearExpired();
        return cache.remove(key);
    }

    /**
     * @return количество элементов ключ-значение в кэше
     */
    public int size() {
        clearExpired();
        return cache.size();
    }

    /**
     * @return возвращает текущее время.
     */
    private Long timeIsNow() {
        return Clock.getTime();
    }
}